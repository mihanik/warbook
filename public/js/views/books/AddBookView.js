define([
  'jquery',
  'underscore',
  'parse',
  'models/category/CategoryModel',
  'collections/category/CategoryCollection',
  'models/book/Book',
  'text!templates/books/AddBookTemplate.html',
  'text!templates/category/categorySelectTemplate.html',
], function($, _, Parse, CategoryModel, CategoryCollection, Book, BookItemTemplate,categorySelectTemplate){

var AddBookView = Parse.View.extend({
    el: "#warbook-content",
    events: {
        'click .submit-add-book'                    : 'submit',

        'click #new-book-epub-uploadbutton'         : 'uploadFile', 
        'click #new-book-cover-uploadbutton'        : 'uploadFile',       
        'click #new-book-rotation-uploadbutton'     : 'uploadFile',

        'change #new-cover-fileselect'              : 'newCoverImage',
        'change #new-cover-mdpi-fileselect'         : 'newCoverImage',
        'change #new-cover-hdpi-fileselect'         : 'newCoverImage',
        'change #new-cover-xhdpi-fileselect'        : 'newCoverImage',
        'change #new-cover-xxhdpi-fileselect'       : 'newCoverImage',
        'change #new-book-rotation-fileselect'      : 'newCoverImage',


        'click #new-book-cover-img'                 : 'openFileSelectDialog', 
        'click #new-book-cover-img-mdpi'            : 'openFileSelectDialog', 
        'click #new-book-cover-img-hdpi'            : 'openFileSelectDialog', 
        'click #new-book-cover-img-xhdpi'           : 'openFileSelectDialog', 
        'click #new-book-cover-img-xxhdpi'          : 'openFileSelectDialog',        
        'click #new-book-rotation-img'              : 'openFileSelectDialog',

        "change #edit-book-genre"                     : "ganreSelected"
    },    
    initialize: function() {
      _.bindAll(this, "submit","uploadFile", "newCoverImage", "openFileSelectDialog", "addOneCategories", "addAllCategories");
      // //вставляем шаблон таблицы  
      this.$el.html(BookItemTemplate);

      this.categories = new CategoryCollection();
      this.categories.query = new Parse.Query(CategoryModel);
      this.categories.bind('add',     this.addOneCategories);
      this.categories.bind('reset',   this.addAllCategories);
      this.categories.fetch();
    },
    addAllCategories:function(collection, filter){
      //this.categories.each(this.addOne);
      var rate_select_template = _.template( categorySelectTemplate, {categories: this.categories, labelSelected: 'Без жанра' });
      this.$('#new-book-genre').html(rate_select_template);
    },
    addOneCategories:function(categiry){

    },
    ganreSelected:function(e){
      this.selectedGanre = $(e.currentTarget).val();
      console.log(this.selectedGanre);
    },
openFileSelectDialog:function(arg)
    {
      var type = $(arg.currentTarget).data('pancakes');
      switch(type) {
          case 'cover-img':
              this.$("#new-cover-fileselect").trigger('click');
              break;
          case 'cover-mdpi':
              this.$("#new-cover-mdpi-fileselect").trigger('click');
              break;
          case 'cover-hdpi':
              this.$("#new-cover-hdpi-fileselect").trigger('click');
              break;  
          case 'cover-xhdpi':
              this.$("#new-cover-xhdpi-fileselect").trigger('click');
              break;
          case 'cover-xxhdpi':
              this.$("#new-cover-xxhdpi-fileselect").trigger('click');
              break;  
          case 'baner-file':
              this.$("#new-book-rotation-fileselect").trigger('click');
              break;
          default:
              console.log(type);
              break;
      }
    },
/////////////////////////////////////////////////////////////////////////////////////////
    newCoverImage:function(e) {
      var files = e.target.files || e.dataTransfer.files;
      // Our file var now holds the selected file
      var type = $(e.currentTarget).data('pancakes');
      switch(type) {
          case 'cover-img':
              this.coverImgFile = files[0];
              this.$("#new-book-cover-img").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-mdpi':
              this.coverImgMdpiFile = files[0];
              this.$("#new-book-cover-img-mdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-hdpi':
              this.coverImgHdpiFile = files[0];
              this.$("#new-book-cover-img-hdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;  
          case 'cover-xhdpi':
              this.coverImgXhdpiFile = files[0];
              this.$("#new-book-cover-img-xhdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-xxhdpi':
              this.coverImgXxdpiFile = files[0];
              this.$("#new-book-cover-img-xxhdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break; 
          case 'epub-file':
              console.log("dsfsdgdfgjkdfghjdfgn");
              this.epubFile = files[0];
              break;
          case 'baner-file':
              this.banerImageFile = files[0];
              this.$("#new-book-rotation-img").attr('src',URL.createObjectURL(event.target.files[0]));
              break; 

          default:
              //console.log(type);
              break;
      }
      //this.$("#new-book-cover-img").attr('src',URL.createObjectURL(event.target.files[0]));
      //this.$("#new-book-cover-uploadbutton").removeAttr("disabled");
    },
/////////////////////////////////////////////////////////////////////////////////////////    
    uploadFile:function(arg)
    {
      var type = $(arg.currentTarget).data('pancakes');
      switch(type) {
          case 'epub-file':
              if(this.epubFile)
              {
                alert("EPUB");
                // this.epubFile = new Parse.File(this.file.name, this.file);
                // this.epubFile.save().then(function(epubFile) 
                //   { 
                //     this.epubFile = epubFile;
                //   }, 
                //   function(error) 
                //   {
                //     alert("error");
                //   }
                // );
              }
              
              break;
          case 'baner-file':
              if(this.banerImageFile!=null)
              {
                  this.banerImageParseFile = new Parse.File(this.banerImageFile.name, this.banerImageFile);
                  this.banerImageParseFile.save().then(function(file) 
                    { 
                      this.banerImageParseFile = file;
                      console.log("baner uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              break;
          case 'cover':
              console.log();
              if(this.coverImgFile!=null)
              {
                  this.coverImgParseFile = new Parse.File(this.coverImgFile.name, this.coverImgFile);
                  this.coverImgParseFile.save().then(function(file) 
                    { 
                      this.coverImgParseFile = file;
                      console.log("coverImgFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgMdpiFile!=null)
              {
                this.coverImgMdpiParseFile = new Parse.File(this.coverImgMdpiFile.name, this.coverImgMdpiFile);
                  this.coverImgMdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgMdpiParseFile = file;
                      console.log("coverImgMdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgHdpiFile!=null)
              {
                this.coverImgHdpiParseFile = new Parse.File(this.coverImgHdpiFile.name, this.coverImgHdpiFile);
                  this.coverImgHdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgHdpiParseFile = file;
                      console.log("coverImgHdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgXhdpiFile!=null)
              {
                this.coverImgXhdpiParseFile = new Parse.File(this.coverImgXhdpiFile.name, this.coverImgXhdpiFile);
                  this.coverImgXhdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgXhdpiParseFile = file;
                      console.log("coverImgXhdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              } 
              if(this.coverImgXxdpiFile!=null)
              {
                this.coverImgXxdpiParseFile = new Parse.File(this.coverImgXxdpiFile.name, this.coverImgXxdpiFile);
                  this.coverImgXxdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgXxdpiParseFile = file;
                      console.log("coverImgXxdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }

              break;              
          default:
              console.log("Unknow type = " + type);
              break;
      }
    },
 
    submit: function(e){

       var dataArray = [];
          dataArray["title"]=this.$("#new-book-title").val();
          dataArray["author"]=this.$("#new-book-author").val();
          dataArray["year"]=this.$("#new-book-year").val();
          dataArray["description"]=this.$("#new-book-description").val();
          dataArray["genre"]=this.$("#new-book-genre").val();
          dataArray["theme"]=this.$("#new-book-theme").val();
          dataArray["price"]=this.$("#new-book-price").val();
          dataArray["rotation"]=this.$("#new-book-rotation").val();
          dataArray["epubFile"]=this.epubParseFile;
          dataArray["banerFile"]=this.banerImageParseFile;
          dataArray["coverFile"]  =this.coverImgParseFile;
          dataArray["coverMdpi"]  =this.coverImgMdpiParseFile;
          dataArray["coverHdpi"]  =this.coverImgHdpiParseFile;
          dataArray["coverXhdpi"] =this.coverImgXhdpiParseFile;
          dataArray["coverXxhdpi"]=this.coverImgXxdpiParseFile;
          dataArray["paper"]=this.$("#new-book-paper").prop('checked');
          dataArray["paperPrice"]=this.$("#new-book-paper-price").val();
          dataArray["weight"]=this.$("#new-book-weight").val();
          dataArray["art"]=this.$("#new-book-art-pack").prop('checked');
          dataArray["artPrice"]=this.$("#new-book-art-price").val();
          dataArray["audio"]=this.$("#new-book-audio-pack").prop('checked');
          dataArray["audioPrice"]=this.$("#new-book-audio-pack-price").val();
          dataArray["video"]=this.$("#new-book-video-pack").prop('checked');
          dataArray["videoPrice"]=this.$("#new-book-video-pack-price").val();

        
        console.log(dataArray);
        // Create a new instance of Book
        var book = new Book();

        //book.create(title, author, description, genre, price, cover);
        book.create(dataArray);
    }
});

  return AddBookView;
  
});