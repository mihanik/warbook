define([
  'jquery',
  'underscore',
  'parse',
  'views/books/EditBookView',
  'text!templates/books/BookItemTemplate.html'
], function($, _, Parse, EditBookView, BookItemTemplate){

var BookView = Parse.View.extend({
  tagName:  "tr",
  events:
  {
    //'click a.edit-book'       : 'editBook',
    'click .edit-book'        : 'editBook',
    'click a.show-book-info'  : 'showbook'
  },
  initialize: function() {
    _.bindAll(this, 'render', 'editBook', 'showbook');
  },
  render: function() {
    var str = this.model.get('description').substring(0, 32);
    this.model.set('description',str+' ...');
    $(this.el).html(_.template( BookItemTemplate, this.model.toJSON() ));
    return this;
  },
  editBook: function(){
    new EditBookView({model:this.model});
    //console.log(editbook);
  },
  showbook:function()
  {
    console.log('View All About Book');
    //new BookViewFull({model:this.model});
  }
});

  return BookView;
  
});
