define([
  'jquery',
  'underscore',
  'parse',
  'router',
  'models/category/CategoryModel',
  'collections/category/CategoryCollection',
  'text!templates/books/EditBookTemplate.html',
  'text!templates/category/categorySelectTemplate.html',  
], function($, _, Parse, Router, CategoryModel, CategoryCollection, EditBookTemplate,categorySelectTemplate){


var EditBookView = Parse.View.extend({
    el: "#warbook-content",
    events: {
        'submit form.edit-book-form'                     : 'submitUpdate',
        //'click .submit-edit-book'                    : 'submitUpdate',

        'click #edit-book-epub-uploadbutton'         : 'uploadFile', 
        'click #edit-book-cover-uploadbutton'        : 'uploadFile',       
        'click #edit-book-rotation-uploadbutton'     : 'uploadFile',

        'change #edit-cover-fileselect'              : 'editCoverImage',
        'change #edit-cover-mdpi-fileselect'         : 'editCoverImage',
        'change #edit-cover-hdpi-fileselect'         : 'editCoverImage',
        'change #edit-cover-xhdpi-fileselect'        : 'editCoverImage',
        'change #edit-cover-xxhdpi-fileselect'       : 'editCoverImage',
        'change #edit-book-rotation-fileselect'      : 'editCoverImage',

        'click #edit-book-cover-img'                 : 'openFileSelectDialog', 
        'click #edit-book-cover-img-mdpi'            : 'openFileSelectDialog', 
        'click #edit-book-cover-img-hdpi'            : 'openFileSelectDialog', 
        'click #edit-book-cover-img-xhdpi'           : 'openFileSelectDialog', 
        'click #edit-book-cover-img-xxhdpi'          : 'openFileSelectDialog',        
        'click #edit-book-rotation-img'              : 'openFileSelectDialog',

        "change #edit-book-genre"                     : "ganreSelected"

    },
    //template: _.template($("#edit-book-template").html()),    
    initialize: function() {
      _.bindAll(this, "submitUpdate", "uploadFile","editCoverImage", "addOneCategories", "addAllCategories");
      //this.$el.html("");

      // this.model.set('coverMdpi',null);
      // this.model.set('coverHdpi',null);
      // this.model.set('coverXhdpi',null);
      // this.model.set('coverXxhdpi',null);
                   
      this.coverImgParseFile = this.model.get("cover");
      this.coverImgMdpiParseFile = this.model.get("coverMdpi");
      this.coverImgHdpiParseFile = this.model.get("coverHdpi");
      this.coverImgXhdpiParseFile = this.model.get("coverXhdpi");
      this.coverImgXxdpiParseFile = this.model.get("coverXxhdpi");
      this.epubParseFile = this.model.get("epubFile");
      this.banerImageParseFile = this.model.get("rotationImage");
      this.selectedGanre = this.model.get("genre");
      this.$el.html(_.template( EditBookTemplate, this.model.toJSON()));

      this.categories = new CategoryCollection();
      this.categories.query = new Parse.Query(CategoryModel);
      this.categories.bind('add',     this.addOneCategories);
      this.categories.bind('reset',   this.addAllCategories);
      this.categories.fetch();

      // var rate_select_template = _.template( categorySelectTemplate, {rates: this.rates, labelValue: 'Something' });
      // $('#edit-book-genre').html(rate_select_template);
    },
    addAllCategories:function(collection, filter){
      //this.categories.each(this.addOne);
      var rate_select_template = _.template( categorySelectTemplate, {categories: this.categories, labelSelected: this.selectedGanre });
      this.$('#edit-book-genre').html(rate_select_template);
    },
    addOneCategories:function(categiry){

    },
    ganreSelected:function(e){
      this.selectedGanre = $(e.currentTarget).val();
      console.log(this.selectedGanre);
    },
    render:function(){
      this.$el.html("");
      console.log('render edit');
      // this.$el.html(this.template(this.model.toJSON()));
    },
    ///////////////////////////

    openFileSelectDialog:function(arg)
    {
      var type = $(arg.currentTarget).data('pancakes');
      switch(type) {
          case 'cover-img':
              this.$("#edit-cover-fileselect").trigger('click');
              break;
          case 'cover-mdpi':
              this.$("#edit-cover-mdpi-fileselect").trigger('click');
              break;
          case 'cover-hdpi':
              this.$("#edit-cover-hdpi-fileselect").trigger('click');
              break;  
          case 'cover-xhdpi':
              this.$("#edit-cover-xhdpi-fileselect").trigger('click');
              break;
          case 'cover-xxhdpi':
              this.$("#edit-cover-xxhdpi-fileselect").trigger('click');
              break;  
          case 'baner-file':
              this.$("#edit-book-rotation-fileselect").trigger('click');
              break;
          default:
              console.log(type);
              break;
      }
    },
/////////////////////////////////////////////////////////////////////////////////////////
    editCoverImage:function(e) {
      var files = e.target.files || e.dataTransfer.files;
      // Our file var now holds the selected file
      var type = $(e.currentTarget).data('pancakes');
      switch(type) {
          case 'cover-img':
              this.coverImgFile = files[0];
              this.$("#edit-book-cover-img").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-mdpi':
              this.coverImgMdpiFile = files[0];
              this.$("#edit-book-cover-img-mdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-hdpi':
              this.coverImgHdpiFile = files[0];
              this.$("#edit-book-cover-img-hdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;  
          case 'cover-xhdpi':
              this.coverImgXhdpiFile = files[0];
              this.$("#edit-book-cover-img-xhdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break;
          case 'cover-xxhdpi':
              this.coverImgXxdpiFile = files[0];
              this.$("#edit-book-cover-img-xxhdpi").attr('src',URL.createObjectURL(event.target.files[0]));
              break; 
          case 'epub-file':
              console.log("dsfsdgdfgjkdfghjdfgn");
              this.epubFile = files[0];
              break;
          case 'baner-file':
              this.banerImageFile = files[0];
              this.$("#edit-book-rotation-img").attr('src',URL.createObjectURL(event.target.files[0]));
              break; 

          default:
              //console.log(type);
              break;
      }
      //this.$("#edit-book-cover-img").attr('src',URL.createObjectURL(event.target.files[0]));
      //this.$("#edit-book-cover-uploadbutton").removeAttr("disabled");
    },
/////////////////////////////////////////////////////////////////////////////////////////    
    uploadFile:function(arg)
    {
      var type = $(arg.currentTarget).data('pancakes');
      switch(type) {
          case 'epub-file':
              if(this.epubFile)
              {
                alert("EPUB");
                // this.epubFile = new Parse.File(this.file.name, this.file);
                // this.epubFile.save().then(function(epubFile) 
                //   { 
                //     this.epubFile = epubFile;
                //   }, 
                //   function(error) 
                //   {
                //     alert("error");
                //   }
                // );
              }
              
              break;
          case 'baner-file':
              if(this.banerImageFile!=null)
              {
                  this.banerImageParseFile = new Parse.File(this.banerImageFile.name, this.banerImageFile);
                  this.banerImageParseFile.save().then(function(file) 
                    { 
                      this.banerImageParseFile = file;
                      console.log("baner uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              break;
          case 'cover':
              console.log();
              if(this.coverImgFile!=null)
              {
                  this.coverImgParseFile = new Parse.File(this.coverImgFile.name, this.coverImgFile);
                  this.coverImgParseFile.save().then(function(file) 
                    { 
                      this.coverImgParseFile = file;
                      console.log("coverImgFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgMdpiFile!=null)
              {
                this.coverImgMdpiParseFile = new Parse.File(this.coverImgMdpiFile.name, this.coverImgMdpiFile);
                  this.coverImgMdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgMdpiParseFile = file;
                      console.log("coverImgMdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgHdpiFile!=null)
              {
                this.coverImgHdpiParseFile = new Parse.File(this.coverImgHdpiFile.name, this.coverImgHdpiFile);
                  this.coverImgHdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgHdpiParseFile = file;
                      console.log("coverImgHdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }
              if(this.coverImgXhdpiFile!=null)
              {
                this.coverImgXhdpiParseFile = new Parse.File(this.coverImgXhdpiFile.name, this.coverImgXhdpiFile);
                  this.coverImgXhdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgXhdpiParseFile = file;
                      console.log("coverImgXhdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              } 
              if(this.coverImgXxdpiFile!=null)
              {
                this.coverImgXxdpiParseFile = new Parse.File(this.coverImgXxdpiFile.name, this.coverImgXxdpiFile);
                  this.coverImgXxdpiParseFile.save().then(function(file) 
                    { 
                      this.coverImgXxdpiParseFile = file;
                      console.log("coverImgXxdpiFile uploaded to server");
                    }, 
                    function(error) 
                    {
                      alert("error");
                    }
                  );
              }

              break;              
          default:
              console.log("Unknow type = " + type);
              break;
      }
    },



    
/////////////////////////////////////////////////////////////////////////////////////////
    submitUpdate: function(e){
      var self = this;
        //e.preventDefault();
       var dataArray = [];
          dataArray["title"]=this.$("#edit-book-title").val();
          dataArray["author"]=this.$("#edit-book-author").val();
          dataArray["year"]=this.$("#edit-book-year").val();
          dataArray["description"]=this.$("#edit-book-description").val();
          dataArray["genre"]=this.selectedGanre
          dataArray["theme"]=this.$("#edit-book-theme").val();
          dataArray["price"]=this.$("#edit-book-price").val();
          dataArray["rotation"]=this.$("#edit-book-rotation").val();
          dataArray["epubFile"]=this.epubParseFile;
          dataArray["banerFile"]=this.banerImageParseFile;
          dataArray["coverFile"]  =this.coverImgParseFile;
          dataArray["coverMdpi"]  =this.coverImgMdpiParseFile;
          dataArray["coverHdpi"]  =this.coverImgHdpiParseFile;
          dataArray["coverXhdpi"] =this.coverImgXhdpiParseFile;
          dataArray["coverXxhdpi"]=this.coverImgXxdpiParseFile;
          dataArray["paper"]=this.$("#edit-book-paper").prop('checked');
          dataArray["paperPrice"]=this.$("#edit-book-paper-price").val();
          dataArray["weight"]=this.$("#edit-book-weight").val();
          dataArray["art"]=this.$("#edit-book-art-pack").prop('checked');
          dataArray["artPrice"]=this.$("#edit-book-art-price").val();
          dataArray["audio"]=this.$("#edit-book-audio-pack").prop('checked');
          dataArray["audioPrice"]=this.$("#edit-book-audio-pack-price").val();
          dataArray["video"]=this.$("#edit-book-video-pack").prop('checked');
          dataArray["videoPrice"]=this.$("#edit-book-video-pack-price").val();
        
        this.model.update(dataArray);
       delete self;
       Router.navigate('.', { trigger: true });
       return false;
    }
});

return EditBookView;
  
});