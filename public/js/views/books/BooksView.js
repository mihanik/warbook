define([
  'jquery',
  'underscore',
  'parse',
  'models/book/Book',
  'collections/books/BooksCollection',
  'views/books/book/BookView',
  'text!templates/books/BooksTableTemplate.html'
], function($, _, Parse, Book, BooksCollection, BookView, BooksTableTemplate){

var BooksView = Parse.View.extend({
    el: "#warbook-content", 
    events: {
        'click .submit-add-book': 'submit'
    },  
    initialize: function() {
      _.bindAll(this, 'addOne', 'addAll', 'render', 'submit');
      //вставляем шаблон таблицы  
      this.$el.html(BooksTableTemplate);

      //запрашиваем и вставляем все записи из бызы
      this.booksCollection = new BooksCollection();
      console.log(this.booksCollection);
      this.booksCollection.query = new Parse.Query(Book);
      this.booksCollection.bind('add',     this.addOne);
      this.booksCollection.bind('reset',   this.addAll);
      this.booksCollection.bind('all',     this.render);

      this.booksCollection.fetch();
      // var book1 = new Book({
      //   title: 'Война и Мир', 
      //   author: 'Толстой Л.Н.',
      //   year        : 1905,
      //   description: 'Давным давно 3000', 
      //   genre       : 'science fiction',
      //   theme       : 'theme',
      //   price         : 100,
      //   rotation    : 0,   
      //   epubFile    : null,    
      //   rotationImage: null,
      //   cover       : null,
      //   coverMdpi   : null,
      //   coverHdpi   : null,
      //   coverXhdpi  : null,  
      //   coverXxhdpi : null,              
      //   paper       : true,
      //   paperPrice : 200,
      //   art         : false,
      //   artPrice   : 0,
      //   weight      : 0,
      //   audio       : false,
      //   audioPrice : 0,
      //   video       : false,
      //   videoPrice : 0       
      // });
      // this.booksCollection.bind('add',     this.addOne);
      // this.booksCollection.bind('reset',   this.addAll);
      // this.booksCollection.bind('all',     this.render);
      // this.booksCollection.add(book1);  
    },
    render: function() {
      // console.log("render OK");
      return this;
    },
    // Add all items in the Todos collection at once.
    addAll: function(collection, filter) {
       this.$("#book-content").html("");
       //console.log(this.bookList.toJSON());
       this.booksCollection.each(this.addOne);
    },
    addOne: function(book)
    {
      // //создаем новую модель
      var view = new BookView({model: book});
      // console.log(view);
      // //вставляем представление 
      this.$("#book-content").append(view.render().el);
    },
    submit: function()
    {
        //console.log("Ky ky");
    }  
});

  return BooksView;
  
});










