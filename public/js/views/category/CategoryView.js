define([
  'jquery',
  'underscore',
  'parse',
  'views/books/EditBookView',
  'text!templates/category/editCategoryTemplate.html'
], function($, _, Parse, EditBookView, editCategoryTemplate){

var CategoryView = Parse.View.extend({
    tagName:  "tr",
    events:
    {
      'click .delete-category'       : 'deleteCategory',
      'click .edit-category'         : 'editCategory'
    },
    initialize: function() {
      // _.bindAll(this, 'render', 'editBook', 'showbook');
    },
    render: function() {
      $(this.el).html(_.template( editCategoryTemplate, this.model.toJSON() ));
      return this;
    },
    editCategory: function(){

    },
    deleteCategory: function(){
      console.log('deleteCategory');
      this.model.destroy();
    },    
    showbook:function()
    {
      // console.log('View All About Book');
      //new BookViewFull({model:this.model});
    }
  });

  return CategoryView;
  
});
