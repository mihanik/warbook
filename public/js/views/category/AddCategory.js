define([
  'jquery',
  'underscore',
  'parse',
  'models/category/CategoryModel',
  'collections/category/CategoryCollection',
  'views/category/CategoryView',
  'text!templates/category/AddCategoryTemplate.html'
], function($, _, Parse, CategoryModel, CategoryCollection, CategoryView,AddCategoryTemplate){

var AddCategoryView = Parse.View.extend({
	  el: "#warbook-content",
    events: {
      'click #add-new-category-submit': 'addNewCategory'
    },  
    initialize: function() {
      _.bindAll(this, 'addOne', 'addAll', 'render', 'addNewCategory');
      this.$el.html(_.template(AddCategoryTemplate));
      this.categoryCollection = new CategoryCollection();
      this.categoryCollection.query = new Parse.Query(CategoryModel);
      this.categoryCollection.bind('add',     this.addOne);
      this.categoryCollection.bind('reset',   this.addAll);
      this.categoryCollection.bind('all',     this.render);
      this.categoryCollection.fetch();

      // var category = new Category({name:'test'});
      // this.categoryCollection.add(category);
    },
    addNewCategory:function(){

      var title = this.$("#new_category_title").val();
      if(!title) return;

      var category = new CategoryModel({
        name:title
      });
      this.categoryCollection.add(category);
      category.save(null, {
        success: function(book) {
            alert('You added a new category:');
        },
        error: function(book, error) {
            alert('ERROR:');
        }
    });
    },
    render: function() {
      console.log("render Category");
      return this;
    },
    // Add all items in the Todos collection at once.
    addAll: function(collection, filter) {
      this.$("#category-content").html("");
      this.categoryCollection.each(this.addOne);
    },
    addOne: function(category){
      // console.log(category);
      var view = new CategoryView({model: category});
      this.$("#category-content").append(view.render().el);
    }
});
return AddCategoryView;
});