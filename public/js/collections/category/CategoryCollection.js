define([
  'underscore',
  'parse',
  'models/category/CategoryModel'
], function(_, Parse, CategoryModel){
var CategoryCollection = Parse.Collection.extend({
  // Reference to this collection's model.
   model: CategoryModel,
   initialize : function(models, options) {
   	
   }
});

return CategoryCollection;

});
