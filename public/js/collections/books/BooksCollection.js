define([
  'underscore',
  'parse',
  'models/book/Book'
], function(_, Parse, Book){

var BooksCollection = Parse.Collection.extend({
  // Reference to this collection's model.
   model: Book,
   initialize : function(models, options) {
   	
   }
});

  return BooksCollection;

});

