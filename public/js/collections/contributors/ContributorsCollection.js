define([
  'underscore',
  'parse',
  'models/contributor/ContributorModel',
  'models/book/BookModel'
], function(_, Parse, ContributorModel, BookModel){

  var ContributorsCollection = Parse.Collection.extend({
      
      model: BookModel,

      initialize : function(models, options) {}        
  });

  return ContributorsCollection;

});