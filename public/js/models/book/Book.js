define([
  'underscore',
  'parse',
  'router'
], function(_, Parse, Router) {
  
  //модель книги
var Book = Parse.Object.extend("Book",{
  defaults: {
    title       : 'name ...',
    author      : 'autor ...',
    year        : 1905,
    description : 'description ...',
    genre       : 'Без жанра',
    theme       : 'theme',
    price       : 0,
    rotation    : 0,
    epubFile    : Object,    
    rotationImage: Object,
    cover       : Object,
    coverMdpi   : Object,
    coverHdpi   : Object,
    coverXhdpi  : Object,  
    coverXxhdpi : Object,              
    paper       : false,
    paperPrice : 0,
    art         : false,
    artPrice   : 0,
    weight      : 0,
    audio       : false,
    audioPrice : 0,
    video       : false,
    videoPrice : 0
  },
  validate: function( attrs ) {
  },
  create:function(dataArray)  
  {
    this.set({
        'title'        : dataArray["title"],
        'author'       : dataArray["author"],
        'year'         : dataArray["year"],
        'description'  : dataArray["description"],
        'genre'        : dataArray["genre"],
        'theme'        : dataArray["theme"],
        'price'        : dataArray["price"],
        'rotation'     : dataArray["rotation"],
        'epubFile'     : dataArray["epubFile"]== undefined ? null : dataArray["epubFile"],
        'rotationImage': dataArray["banerFile"]== undefined ? null : dataArray["banerFile"],
        'cover'        : dataArray["coverFile"]== undefined ? null : dataArray["coverFile"],
        'coverMdpi'    : dataArray["coverMdpi"]== undefined ? null : dataArray["coverMdpi"],
        'coverHdpi'    : dataArray["coverHdpi"]== undefined ? null : dataArray["coverHdpi"],
        'coverXhdpi'   : dataArray["coverXhdpi"]== undefined ? null : dataArray["coverXhdpi"],
        'coverXxhdpi'  : dataArray["coverXxhdpi"]== undefined ? null : dataArray["coverXxhdpi"],
        'paper'        : dataArray["paper"],
        'paperPrice'   : dataArray["paperPrice"],
        'weight'       : dataArray["weight"],
        'art'          : dataArray["art"],
        'artPrice'     : dataArray["artPrice"],
        'audio'        : dataArray["audio"],
        'audioPrice'   : dataArray["audioPrice"],
        'video'        : dataArray["video"],
        'videoPrice'   : dataArray["videoPrice"]
    }).save(null, {
        success: function(book) {
            alert('You added a new book: ' + book.get('title'));

            // console.log(book.toJSON());
        },
        error: function(book, error) {
            console.log(book);
            console.log(error);
        }
    });
  },

  update:function(dataArray){
    this.set({
        'title'        : dataArray["title"],
        'author'       : dataArray["author"],
        'year'         : dataArray["year"],
        'description'  : dataArray["description"],
        'genre'        : dataArray["genre"],
        'theme'        : dataArray["theme"],
        'price'        : dataArray["price"],
        'rotation'     : dataArray["rotation"],
        'epubFile'     : dataArray["epubFile"]== undefined ? null : dataArray["epubFile"],
        'rotationImage': dataArray["banerFile"]== undefined ? null : dataArray["banerFile"],
        'cover'        : dataArray["coverFile"]== undefined ? null : dataArray["coverFile"],
        'coverMdpi'    : dataArray["coverMdpi"]== undefined ? null : dataArray["coverMdpi"],
        'coverHdpi'    : dataArray["coverHdpi"]== undefined ? null : dataArray["coverHdpi"],
        'coverXhdpi'   : dataArray["coverXhdpi"]== undefined ? null : dataArray["coverXhdpi"],
        'coverXxhdpi'  : dataArray["coverXxhdpi"]== undefined ? null : dataArray["coverXxhdpi"],
        'paper'        : dataArray["paper"],
        'paperPrice'   : dataArray["paperPrice"],
        'weight'       : dataArray["weight"],
        'art'          : dataArray["art"],
        'artPrice'     : dataArray["artPrice"],
        'audio'        : dataArray["audio"],
        'audioPrice'   : dataArray["audioPrice"],
        'video'        : dataArray["video"],
        'videoPrice'   : dataArray["videoPrice"]       
    }).save(null, {
        success: function(book) {
            alert('You book: ' + book.get('title') + 'has been updated');
            //Router.navigate('.', { trigger: true });
        },
        error: function(book, error) {
            console.log(book);
            console.log(error);
        }
    });
  }


});

return Book;

});