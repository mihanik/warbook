define([
  'underscore',
  'parse',
], function(_, Parse) {

  var CategoryModel = Parse.Object.extend("CategoryModel",{

  	defaults : {
  		name : 'empty'
  	},
  validate: function( attrs ) {
  },
  create:function(dataArray)  
  {
    this.set({
        'name'        : dataArray["name"]
    }).save(null, {
        success: function(category) {
            alert('You added a new category: ' + category.get('name'));
            // console.log(book.toJSON());
        },
        error: function(category, error) {
            console.log(category);
            console.log(error);
        }
    });
  },
  update:function(dataArray){
    this.set({
        'title'        : dataArray["title"],
    }).save(null, {
        success: function(category) {
            alert('You category: ' + category.get('name') + 'has been updated');
        },
        error: function(category, error) {
            console.log(category);
            console.log(error);
        }
    });
  },
  delete:function()
  {
    
  }
  });

  return CategoryModel;

});