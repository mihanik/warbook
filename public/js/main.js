// Author: Thomas Davis <thomasalwyndavis@gmail.com>
// Filename: main.js

// Require.js allows us to configure shortcut alias
// Their usage will become more apparent futher along in the tutorial.
require.config({
  paths: {
    jquery: 'libs/jquery/jquery-2.0.3.min',
    underscore: 'libs/underscore/underscore',
    parse: 'libs/parse/parse-1.2.13.min',
    //parse: 'libs/parse/parse-1.6.7.min',
    templates: '../templates'
  },
  shim: {
      jquery: {
        exports: '$'
      },
      underscore: {
        exports: '_'
      },
      parse: {
        deps: ['underscore','jquery'],
        exports: 'Parse'
      }
  }

});

require([
  // Load our app module and pass it to our definition function
  'app',
], function(App){
  // The "app" dependency is passed in as "App"
  // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
  App.initialize();
});
