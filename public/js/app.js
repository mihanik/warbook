// Filename: app.js
define([
  'jquery', 
  'underscore', 
  'parse',
  'router', // Request router.js
], function($, _, Parse, Router){
  var initialize = function(){
    Parse.$ = jQuery;
    Parse.initialize("14R5V1WDXh9fC3qptkD3t7AyFs2kNgiYlg2gHtgM", "Bi0xqrvUz8ynr4XX1LQK5thgNj42bsnEv6hSz6x6");
    Router.initialize();
  };

  return { 
    initialize: initialize
  };
});
