// Filename: router.js
define([
  'jquery',
  'underscore',
  'parse',
  'views/books/BooksView',
  'views/books/AddBookView',
  'views/category/AddCategory'
], function($, _, Parse, BooksView, AddBookView, AddCategory) {
  
   var AppRouter = Parse.Router.extend({
    routes: {      
      "addnewbook"  : "addnewbook",
      "category" : "category",
      // Default
      '': 'defaultAction'
    }
   });
  
  var initialize = function(){

    var app_router = new AppRouter();
    
    app_router.on('route:addnewbook', function () {
      var addBookView = new AddBookView();     
    });

    app_router.on('route:category', function () {
      // console.log("Add Category");
      var addCategory = new AddCategory();     
    });

    app_router.on('route:defaultAction', function (actions) {
        var homeView = new BooksView();
        homeView.render();
        // console.log(homeView);
    });
   

    Parse.history.start();
  };
  return { 
    initialize: initialize
  };
});
